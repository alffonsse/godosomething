package main

import "time"

type Project struct {
	Title string
	Tasks []Task
}

type Task struct {
	Title    string
	DueDate  time.Time
	Done     bool
	SubTasks []SubTask
}

type SubTask struct {
	Title string
	Done  bool
}
