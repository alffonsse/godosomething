module gitlab.com/alffonsse/godosomething

go 1.16

require (
	github.com/fatih/color v1.10.0
	github.com/jroimartin/gocui v0.4.0
	github.com/nsf/termbox-go v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5
)
