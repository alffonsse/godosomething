package main

import (
	"fmt"
	"os"

	"github.com/jroimartin/gocui"
	flag "github.com/spf13/pflag"
)

const version = "v0.3.0"

var (
	currentView    = "Projects"
	currentProject = -1
	currentTask    = -1
	currentSubTask = -1
	storage        Storage
	dataFile       string
)

func main() {
	flag.StringVarP(&dataFile, "file", "f", getDefaultStoragePath(), "Path to the file to load")
	doPrintVersion := flag.BoolP("version", "v", false, "Print the version and exit")
	flag.Parse()

	if *doPrintVersion {
		printVersion()
	}

	if err := loadData(dataFile); err != nil {
		exitWithError(nil, err.Error(), 1)
	}

	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		exitWithError(g, err.Error(), 1)
	}
	defer g.Close()

	g.SetManagerFunc(layout)
	g.SelFgColor = gocui.ColorYellow
	g.Highlight = true
	g.InputEsc = true

	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		exitWithError(g, err.Error(), 0)
	}

	if err := layout(g); err != nil {
		exitWithError(g, err.Error(), 1)
	}

	if err := registerKeybindings(g); err != nil {
		exitWithError(g, err.Error(), 1)
	}

	if _, err := g.SetCurrentView("Projects"); err != nil {
		exitWithError(g, err.Error(), 1)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		exitWithError(g, err.Error(), 1)
	}
}

func registerKeybindings(gui *gocui.Gui) error {
	for _, view := range []string{"Projects", "Tasks", "Details"} {
		if err := gui.SetKeybinding(view, gocui.KeyArrowDown, gocui.ModNone, goDown); err != nil {
			return err
		}

		if err := gui.SetKeybinding(view, 'j', gocui.ModNone, goDown); err != nil {
			return err
		}

		if err := gui.SetKeybinding(view, gocui.KeyArrowUp, gocui.ModNone, goUp); err != nil {
			return err
		}

		if err := gui.SetKeybinding(view, 'k', gocui.ModNone, goUp); err != nil {
			return err
		}

		if err := gui.SetKeybinding(view, 'q', gocui.ModNone, quit); err != nil {
			return err
		}

		if err := gui.SetKeybinding(view, '?', gocui.ModNone, displayKeybindingsHelpView); err != nil {
			return err
		}
	}

	// Projects

	if err := gui.SetKeybinding("Projects", gocui.KeyArrowRight, gocui.ModNone, focusTasksView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'l', gocui.ModNone, focusTasksView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'a', gocui.ModNone, newItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'e', gocui.ModNone, editItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'D', gocui.ModNone, deleteProject); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'K', gocui.ModNone, moveCurrentProjectUp); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Projects", 'J', gocui.ModNone, moveCurrentProjectDown); err != nil {
		return err
	}

	// Tasks
	if err := gui.SetKeybinding("Tasks", gocui.KeyArrowLeft, gocui.ModNone, focusProjectsView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", gocui.KeyArrowRight, gocui.ModNone, focusDetailsView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'h', gocui.ModNone, focusProjectsView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'l', gocui.ModNone, focusDetailsView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", gocui.KeySpace, gocui.ModNone, toggleTaskStatus); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'a', gocui.ModNone, newItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'e', gocui.ModNone, editItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'D', gocui.ModNone, deleteTask); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", '@', gocui.ModNone, taskDueDateInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'K', gocui.ModNone, moveCurrentTaskUp); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Tasks", 'J', gocui.ModNone, moveCurrentTaskDown); err != nil {
		return err
	}

	// Details
	if err := gui.SetKeybinding("Details", gocui.KeyArrowLeft, gocui.ModNone, focusTasksView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'h', gocui.ModNone, focusTasksView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", gocui.KeySpace, gocui.ModNone, toggleSubTaskStatus); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'a', gocui.ModNone, newItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'e', gocui.ModNone, editItemInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'D', gocui.ModNone, deleteSubTask); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", '@', gocui.ModNone, taskDueDateInputView); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'K', gocui.ModNone, moveCurrentSubTaskUp); err != nil {
		return err
	}

	if err := gui.SetKeybinding("Details", 'J', gocui.ModNone, moveCurrentSubTaskDown); err != nil {
		return err
	}

	return nil
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	fourth := maxX / 4
	paddingX := 0
	paddingY := 1

	pv, err := g.SetView("Projects", paddingX, 0, fourth-paddingX, maxY-paddingY)
	if err != nil {
		if err == gocui.ErrUnknownView {
			pv.SelFgColor = gocui.ColorYellow
			if err := updateProjectsView(g, pv); err != nil {
				return err
			}
			if len(storage.Projects) > 0 {
				currentProject = 0
			}
		} else {
			return err
		}
	}
	pv.Highlight = true

	tv, err := g.SetView("Tasks", fourth+1, 0, fourth*2, maxY-paddingY)
	if err != nil {
		if err == gocui.ErrUnknownView {
			tv.SelFgColor = gocui.ColorYellow
			if err := updateTasksView(g, tv); err != nil {
				return err
			}
		} else {
			return err
		}
	}
	if currentView != pv.Name() {
		tv.Highlight = true
	} else {
		tv.Highlight = false
	}

	dv, err := g.SetView("Details", fourth*2+1, 0, maxX-1, maxY-paddingY)
	if err != nil {
		if err == gocui.ErrUnknownView {
			dv.SelFgColor = gocui.ColorYellow
		} else {
			return err
		}
	}
	if currentView == dv.Name() {
		dv.Highlight = true
	} else {
		dv.Highlight = false
	}

	return nil
}

func printVersion() {
	fmt.Println(version)
	os.Exit(0)
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func exitWithError(gui *gocui.Gui, message string, code int) {
	if gui != nil {
		gui.Close()
	}

	fmt.Println(message)

	if code < 0 {
		code = 1
	}

	os.Exit(code)
}
