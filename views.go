package main

import (
	"fmt"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/fatih/color"

	"github.com/jroimartin/gocui"
)

func goDown(gui *gocui.Gui, view *gocui.View) error {
	_, maxY := view.Size()
	x, y := view.Cursor()
	line, _ := view.Line(y)
	if line == "" {
		// If we are in the details view and there is no sub-task we could be on an empty line
		return nil
	}

	switch view.Name() {
	case "Projects":
		if currentProject+1 >= len(storage.Projects) {
			// No more projects
			return nil
		}

		break
	case "Tasks":
		if currentTask+1 >= len(storage.Projects[currentProject].Tasks) {
			// No more tasks
			return nil
		}

		break
	case "Details":
		if currentSubTask+1 >= len(storage.Projects[currentProject].Tasks[currentTask].SubTasks) {
			// No more sub-tasks
			return nil
		}

		break
	}

	if y+1 == maxY {
		// Try to scroll down
		xo, yo := view.Origin()
		if err := view.SetOrigin(xo, yo+1); err != nil {
			return err
		}
	} else {
		if err := view.SetCursor(x, y+1); err != nil {
			return err
		}
	}

	switch view.Name() {
	case "Projects":
		currentProject++
		currentTask = -1

		// reset scroll of the tasks view
		tv, _ := gui.View("Tasks")
		_ = tv.SetOrigin(0, 0)
		_ = tv.SetCursor(0, 0)
		return updateTasksView(gui, tv)
	case "Tasks":
		currentTask++
		currentSubTask = -1

		// reset scroll of the details view
		dv, _ := gui.View("Details")
		_ = dv.SetOrigin(0, 0)
		_ = dv.SetCursor(0, 0)
		return updateTaskDetailsView(gui, dv)
	case "Details":
		currentSubTask++
		break
	}

	return nil
}

func goUp(gui *gocui.Gui, view *gocui.View) error {
	minSelectableLine := 0
	x, y := view.Cursor()
	line, _ := view.Line(y)
	if line == "" {
		// If we are in the details view and there is no sub-task we could be on an empty line
		return nil
	}

	if view.Name() == "Details" {
		// First line of the details view is infos about the current task, we do not want to be able to select this line
		minSelectableLine = 1
	}

	switch view.Name() {
	case "Projects":
		if currentProject-1 < 0 {
			// No more projects
			return nil
		}

		break
	case "Tasks":
		if currentTask-1 < 0 {
			// No more tasks
			return nil
		}

		break
	case "Details":
		if currentSubTask-1 < 0 {
			// No more sub-tasks
			return nil
		}

		break
	}

	if y-1 < minSelectableLine {
		// Try to scroll up
		xo, yo := view.Origin()
		if err := view.SetOrigin(xo, yo-1); err != nil {
			return err
		}
	} else {
		if err := view.SetCursor(x, y-1); err != nil {
			return err
		}
	}

	switch view.Name() {
	case "Projects":
		currentProject--
		currentTask = -1

		// reset scroll of the tasks view
		tv, _ := gui.View("Tasks")
		_ = tv.SetOrigin(0, 0)
		_ = tv.SetCursor(0, 0)
		return updateTasksView(gui, tv)
	case "Tasks":
		currentTask--
		currentSubTask = -1

		// reset scroll of the details view
		dv, _ := gui.View("Details")
		_ = dv.SetOrigin(0, 0)
		_ = dv.SetCursor(0, 0)
		return updateTaskDetailsView(gui, dv)
	case "Details":
		currentSubTask--
		break
	}

	return nil
}

func focusProjectsView(gui *gocui.Gui, _ *gocui.View) error {
	_, err := gui.SetCurrentView("Projects")
	if err != nil {
		return err
	}

	currentView = "Projects"

	dv, _ := gui.View("Details")
	dv.Clear()

	return nil
}

func focusTasksView(gui *gocui.Gui, view *gocui.View) error {
	if len(storage.Projects) == 0 {
		// Don't focus the tasks view if there are no projects
		return nil
	}

	_, err := gui.SetCurrentView("Tasks")
	if err != nil {
		return err
	}

	currentView = "Tasks"
	if view.Name() == "Projects" {
		currentSubTask = -1
		if len(storage.Projects[currentProject].Tasks) > 0 {
			if currentTask < 0 {
				currentTask = 0
			}
			dv, _ := gui.View("Details")
			return updateTaskDetailsView(gui, dv)
		}
	}

	return nil
}

func focusDetailsView(gui *gocui.Gui, _ *gocui.View) error {
	if len(storage.Projects[currentProject].Tasks) == 0 {
		// Don't focus the details view if there are no tasks in the current project
		return nil
	}

	dv, err := gui.SetCurrentView("Details")
	if err != nil {
		return err
	}

	if currentSubTask < 0 {
		if err := dv.SetCursor(0, 1); err != nil {
			return err
		}
		if err := dv.SetOrigin(0, 0); err != nil {
			return err
		}
		if len(storage.Projects[currentProject].Tasks[currentTask].SubTasks) > 0 {
			currentSubTask = 0
		}
	}

	currentView = "Details"

	return nil
}

func updateProjectsView(_ *gocui.Gui, view *gocui.View) error {
	view.Clear()
	for _, project := range storage.Projects {
		line := project.Title

		if len(project.Tasks) > 0 {
			tasksDone := 0
			for _, task := range project.Tasks {
				if task.Done {
					tasksDone++
				}
			}
			line = line + fmt.Sprintf(" [%d/%d]", tasksDone, len(project.Tasks))
		}

		_, err := fmt.Fprintln(view, line)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateTasksView(gui *gocui.Gui, view *gocui.View) error {
	view.Clear()

	if currentProject < 0 {
		return nil
	}

	var err error
	tasks := storage.Projects[currentProject].Tasks

	if len(tasks) == 0 {
		dv, err := gui.View("Details")
		if err == nil {
			dv.Clear()
		}
		return nil
	}

	for _, task := range tasks {
		line := ""

		if task.Done {
			line = line + "[X] "
		} else {
			line = line + "[ ] "
		}

		line = line + task.Title

		if len(task.SubTasks) > 0 {
			subtasksDone := 0
			for _, subtask := range task.SubTasks {
				if subtask.Done {
					subtasksDone++
				}
			}
			line = line + fmt.Sprintf(" [%d/%d]", subtasksDone, len(task.SubTasks))
		}

		_, err = fmt.Fprintln(view, line)
		if err != nil {
			return err
		}
	}

	return nil
}

func updateTaskDetailsView(_ *gocui.Gui, view *gocui.View) error {
	now := time.Now()
	view.Clear()

	if currentTask < 0 {
		return nil
	}

	var err error
	task := storage.Projects[currentProject].Tasks[currentTask]

	header := ""
	header += color.New(color.Bold).Sprint(task.Title)

	if !task.DueDate.IsZero() {
		if task.DueDate.Before(now) && !task.Done {
			header += color.New(color.FgRed).Sprint(task.DueDate.Format("    02-01-2006"))
		} else {
			header += color.New(color.FgGreen).Sprint(task.DueDate.Format("    02-01-2006"))
		}
	}

	if _, err := fmt.Fprintln(view, header); err != nil {
		return err
	}

	if len(task.SubTasks) == 0 {
		return nil
	}

	for _, subtask := range task.SubTasks {
		if subtask.Done {
			_, err = fmt.Fprintf(view, "[X] %s\n", subtask.Title)
		} else {
			_, err = fmt.Fprintf(view, "[ ] %s\n", subtask.Title)
		}

		if err != nil {
			return err
		}
	}

	return err
}

func toggleTaskStatus(gui *gocui.Gui, view *gocui.View) error {
	if currentTask < 0 {
		return nil
	}

	task := &storage.Projects[currentProject].Tasks[currentTask]
	task.Done = !task.Done
	rearrangeTasks()

	if err := saveData(dataFile); err != nil {
		return err
	}

	pv, _ := gui.View("Projects")
	if err := updateProjectsView(gui, pv); err != nil {
		return err
	}

	dv, _ := gui.View("Details")
	if err := updateTaskDetailsView(gui, dv); err != nil {
		return err
	}

	return updateTasksView(gui, view)
}

func toggleSubTaskStatus(gui *gocui.Gui, view *gocui.View) error {
	if currentSubTask < 0 {
		return nil
	}

	subtask := &storage.Projects[currentProject].Tasks[currentTask].SubTasks[currentSubTask]
	subtask.Done = !subtask.Done
	rearrangeSubTasks()

	if err := saveData(dataFile); err != nil {
		return err
	}

	tv, _ := gui.View("Tasks")
	if err := updateTasksView(gui, tv); err != nil {
		return err
	}

	return updateTaskDetailsView(gui, view)
}

func newItemInputView(gui *gocui.Gui, view *gocui.View) error {
	maxX, maxY := gui.Size()
	var title string
	var name string

	switch view.Name() {
	case "Projects":
		title = "Name of new project"
		name = "addProject"
	case "Tasks":
		title = "Name of new task"
		name = "addTask"
	case "Details":
		title = "Name of new sub-task"
		name = "addSubTask"
	}

	if iv, err := gui.SetView(name, maxX/2-12, maxY/2, maxX/2+12, maxY/2+2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		iv.Title = title
		iv.Editable = true
		gui.Cursor = true
		if _, err := gui.SetCurrentView(name); err != nil {
			return err
		}
		if err := gui.SetKeybinding(name, gocui.KeyEnter, gocui.ModNone, createItem); err != nil {
			return err
		}
		if err := gui.SetKeybinding(name, gocui.KeyEsc, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
			view.Clear()
			return createItem(gui, view)
		}); err != nil {
			return err
		}
	}

	return nil
}

func createProject(gui *gocui.Gui, project Project) error {
	projects := append(storage.Projects, Project{})
	copy(projects[currentProject+2:], projects[currentProject+1:])
	projects[currentProject+1] = project
	storage.Projects = projects

	if err := saveData(dataFile); err != nil {
		return err
	}

	pv, _ := gui.SetCurrentView("Projects")

	// If there was project currentProject is -1, update it to 0 since we are going
	// to focus the new project which will be index 0
	if currentProject < 0 {
		currentProject = 0
	} else {
		if err := goDown(gui, pv); err != nil {
			return err
		}
	}

	tv, _ := gui.View("Tasks")
	if err := updateTasksView(gui, tv); err != nil {
		return err
	}

	return updateProjectsView(gui, pv)
}

func createTask(gui *gocui.Gui, task Task) error {
	tasks := append(storage.Projects[currentProject].Tasks, Task{})
	copy(tasks[currentTask+2:], tasks[currentTask+1:])
	tasks[currentTask+1] = task
	storage.Projects[currentProject].Tasks = tasks

	if err := saveData(dataFile); err != nil {
		return err
	}

	tv, _ := gui.SetCurrentView("Tasks")

	// If there was no task in the project currentTask is -1, update it to 0 since we are going
	// to focus the new task which will be index 0
	if currentTask < 0 {
		currentTask = 0

		// Since this is the first task in this project we need to manually update the details view
		// to display the new task's details. This is done automatically via the goDown() method
		// in the other case.
		dv, _ := gui.View("Details")
		if err := updateTaskDetailsView(gui, dv); err != nil {
			return err
		}
	} else {
		if err := goDown(gui, tv); err != nil {
			return err
		}
	}

	pv, _ := gui.View("Projects")
	if err := updateProjectsView(gui, pv); err != nil {
		return err
	}

	return updateTasksView(gui, tv)
}

func createSubTask(gui *gocui.Gui, subTask SubTask) error {
	subTasks := append(storage.Projects[currentProject].Tasks[currentTask].SubTasks, SubTask{})
	copy(subTasks[currentSubTask+2:], subTasks[currentSubTask+1:])
	subTasks[currentSubTask+1] = subTask
	storage.Projects[currentProject].Tasks[currentTask].SubTasks = subTasks

	if err := saveData(dataFile); err != nil {
		return err
	}

	dv, _ := gui.SetCurrentView("Details")

	// If there was no sub-task for the current task currentSubTask is -1, update it to 0 since
	// we are going to focus the new sub-task which will be index 0
	if currentSubTask < 0 {
		currentSubTask = 0
	} else {
		if err := goDown(gui, dv); err != nil {
			return err
		}
	}

	tv, _ := gui.View("Tasks")
	if err := updateTasksView(gui, tv); err != nil {
		return err
	}

	return updateTaskDetailsView(gui, dv)
}

func createItem(gui *gocui.Gui, view *gocui.View) error {
	newItem := strings.TrimSuffix(view.Buffer(), "\n")
	inputViewName := view.Name()
	_ = gui.DeleteView(inputViewName)
	gui.DeleteKeybindings(inputViewName)
	gui.Cursor = false

	if newItem != "" {
		switch inputViewName {
		case "addProject":
			return createProject(gui, Project{Title: newItem})
		case "addTask":
			return createTask(gui, Task{Title: newItem})
		case "addSubTask":
			return createSubTask(gui, SubTask{Title: newItem})
		}
	}

	_, _ = gui.SetCurrentView(currentView)
	return nil
}

func editItemInputView(gui *gocui.Gui, view *gocui.View) error {
	maxX, maxY := gui.Size()
	var name string

	switch view.Name() {
	case "Projects":
		if currentProject < 0 {
			return nil
		}
		name = "editProject"
	case "Tasks":
		if currentTask < 0 {
			return nil
		}
		name = "editTask"
	case "Details":
		if currentSubTask < 0 {
			return nil
		}
		name = "editSubTask"
	}

	iv, err := gui.SetView(name, maxX/2-12, maxY/2, maxX/2+12, maxY/2+2)
	if err != nil && err != gocui.ErrUnknownView {
		return err
	}

	iv.Editable = true
	gui.Cursor = true
	if _, err := gui.SetCurrentView(name); err != nil {
		return err
	}
	if err := gui.SetKeybinding(name, gocui.KeyEnter, gocui.ModNone, editItem); err != nil {
		return err
	}
	if err := gui.SetKeybinding(name, gocui.KeyEsc, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
		view.Clear()
		return editItem(gui, view)
	}); err != nil {
		return err
	}

	var inputViewContent string
	switch view.Name() {
	case "Projects":
		inputViewContent = storage.Projects[currentProject].Title
	case "Tasks":
		inputViewContent = storage.Projects[currentProject].Tasks[currentTask].Title
	case "Details":
		inputViewContent = storage.Projects[currentProject].Tasks[currentTask].SubTasks[currentSubTask].Title
	}

	_, _ = fmt.Fprint(iv, inputViewContent)
	iv.MoveCursor(utf8.RuneCountInString(inputViewContent), 0, true)

	return nil
}

func editItem(gui *gocui.Gui, view *gocui.View) error {
	newTitle := strings.TrimSuffix(view.Buffer(), "\n")
	inputViewName := view.Name()
	_ = gui.DeleteView(inputViewName)
	gui.DeleteKeybindings(inputViewName)
	gui.Cursor = false

	if newTitle != "" {
		switch inputViewName {
		case "editProject":
			return editProject(gui, newTitle)
		case "editTask":
			return editTask(gui, newTitle)
		case "editSubTask":
			return editSubTask(gui, newTitle)
		}
	}

	_, _ = gui.SetCurrentView(currentView)

	return nil
}

func editProject(gui *gocui.Gui, title string) error {
	view, _ := gui.SetCurrentView(currentView)

	project := storage.Projects[currentProject]

	if project.Title == title {
		return nil
	}

	project.Title = title
	storage.Projects[currentProject] = project

	if err := saveData(dataFile); err != nil {
		return err
	}

	return updateProjectsView(gui, view)
}

func editTask(gui *gocui.Gui, title string) error {
	view, _ := gui.SetCurrentView(currentView)

	task := storage.Projects[currentProject].Tasks[currentTask]

	if task.Title == title {
		return nil
	}

	task.Title = title
	storage.Projects[currentProject].Tasks[currentTask] = task

	if err := saveData(dataFile); err != nil {
		return err
	}

	if err := updateTasksView(gui, view); err != nil {
		return err
	}

	dv, _ := gui.View("Details")
	return updateTaskDetailsView(gui, dv)
}

func editSubTask(gui *gocui.Gui, title string) error {
	view, _ := gui.SetCurrentView(currentView)

	subtask := storage.Projects[currentProject].Tasks[currentTask].SubTasks[currentSubTask]

	if subtask.Title == title {
		return nil
	}

	subtask.Title = title
	storage.Projects[currentProject].Tasks[currentTask].SubTasks[currentSubTask] = subtask

	if err := saveData(dataFile); err != nil {
		return err
	}

	return updateTaskDetailsView(gui, view)
}

func taskDueDateInputView(gui *gocui.Gui, _ *gocui.View) error {
	maxX, maxY := gui.Size()
	name := "editTaskDueDate"
	iv, err := gui.SetView(name, maxX/2-12, maxY/2, maxX/2+12, maxY/2+2)
	if err != nil && err != gocui.ErrUnknownView {
		return err
	}

	if !storage.Projects[currentProject].Tasks[currentTask].DueDate.IsZero() {
		_, _ = fmt.Fprint(iv, storage.Projects[currentProject].Tasks[currentTask].DueDate.Format("02-01-2006"))
	}

	iv.Title = "Task due date"
	iv.Editable = true
	gui.Cursor = true
	if _, err := gui.SetCurrentView(name); err != nil {
		return err
	}
	if err := gui.SetKeybinding(name, gocui.KeyEnter, gocui.ModNone, editTaskDueDate); err != nil {
		return err
	}
	if err := gui.SetKeybinding(name, gocui.KeyEsc, gocui.ModNone, func(gui *gocui.Gui, view *gocui.View) error {
		view.Clear()
		return editTaskDueDate(gui, view)
	}); err != nil {
		return err
	}

	return nil
}

func editTaskDueDate(gui *gocui.Gui, view *gocui.View) error {
	var (
		date time.Time
		err  error
	)

	dateInput := strings.TrimSuffix(view.Buffer(), "\n")
	if dateInput != "" {
		// TODO: handle multiple formats ?
		date, err = time.Parse("02-01-2006", dateInput)
		// TODO: handle parsing error
		if err == nil {
			task := storage.Projects[currentProject].Tasks[currentTask]
			// We store the date at 23:59:59 so that it is not drawn in red before the next day
			task.DueDate = time.Date(date.Year(), date.Month(), date.Day(), 23, 59, 59, 0, date.Location())
			storage.Projects[currentProject].Tasks[currentTask] = task
			if err := saveData(dataFile); err != nil {
				return err
			}
		}
	}

	inputViewName := "editTaskDueDate"
	_ = gui.DeleteView(inputViewName)
	gui.DeleteKeybindings(inputViewName)
	gui.Cursor = false
	_, _ = gui.SetCurrentView(currentView)
	dv, _ := gui.View("Details")
	return updateTaskDetailsView(gui, dv)
}

func displayKeybindingsHelpView(gui *gocui.Gui, view *gocui.View) error {
	maxX, maxY := gui.Size()
	name := "keybindingsHelp"
	iv, err := gui.SetView(name, maxX/2-32, 2, maxX/2+32, maxY-2)
	if err != nil && err != gocui.ErrUnknownView {
		return err
	}

	helptext := "Ctrl+C Quit the app\n" +
		"? Show this help\n" +
		"<- Go to the view on the left\n" +
		"-> Go to the view on the right\n" +
		"\nProjects:\n" +
		"    a Add a new project\n" +
		"    D Delete the current project\n" +
		"    K Move the current project up\n" +
		"    J Move the current project down\n" +
		"\nTasks:\n" +
		"    a Add a new task\n" +
		"    D Delete the current task\n" +
		"    K Move the current task up\n" +
		"    J Move the current task down\n" +
		"    <SPACE> Toggle the current task to do/done status\n" +
		"    @ Set/Edit the current task due date\n" +
		"\nSub-tasks:\n" +
		"    a Add a new sub-task\n" +
		"    D Delete the current sub-task\n" +
		"    K Move the current sub-task up\n" +
		"    J Move the current sub-task down\n" +
		"    <SPACE> Toggle the current sub-task to do/done status\n"

	if _, err := fmt.Fprint(iv, helptext); err != nil {
		return err
	}

	iv.Title = "Keybindings"
	if _, err := gui.SetCurrentView(name); err != nil {
		return err
	}

	if err := gui.SetKeybinding(name, gocui.KeyEsc, gocui.ModNone, destroyKeybindingsHelpView); err != nil {
		return err
	}

	return nil
}

func destroyKeybindingsHelpView(gui *gocui.Gui, _ *gocui.View) error {
	viewName := "keybindingsHelp"
	_ = gui.DeleteView(viewName)
	gui.DeleteKeybindings(viewName)
	_, _ = gui.SetCurrentView(currentView)
	return nil
}

func deleteProject(gui *gocui.Gui, view *gocui.View) error {
	if currentProject < 0 {
		return nil
	}

	storage.Projects = append(storage.Projects[:currentProject], storage.Projects[currentProject+1:]...)
	if err := saveData(dataFile); err != nil {
		return err
	}
	currentProject--
	_ = view.SetCursor(0, 0)
	_ = view.SetOrigin(0, 0)
	if err := updateProjectsView(gui, view); err != nil {
		return err
	}

	tv, _ := gui.View("Tasks")
	if len(storage.Projects) > 0 {
		return updateTasksView(gui, tv)
	}

	tv.Clear()

	return nil
}

func deleteTask(gui *gocui.Gui, view *gocui.View) error {
	if currentTask < 0 {
		return nil
	}

	tasks := append(storage.Projects[currentProject].Tasks[:currentTask], storage.Projects[currentProject].Tasks[currentTask+1:]...)
	storage.Projects[currentProject].Tasks = tasks
	if err := saveData(dataFile); err != nil {
		return err
	}

	if len(storage.Projects[currentProject].Tasks) > 0 {
		currentTask = 0
	} else {
		currentTask = -1
	}

	_ = view.SetCursor(0, 0)
	_ = view.SetOrigin(0, 0)
	if err := updateTasksView(gui, view); err != nil {
		return err
	}

	pv, _ := gui.View("Projects")
	if err := updateProjectsView(gui, pv); err != nil {
		return err
	}

	dv, _ := gui.View("Details")
	return updateTaskDetailsView(gui, dv)
}

func deleteSubTask(gui *gocui.Gui, view *gocui.View) error {
	if currentSubTask < 0 {
		return nil
	}

	subtasks := append(storage.Projects[currentProject].Tasks[currentTask].SubTasks[:currentSubTask], storage.Projects[currentProject].Tasks[currentTask].SubTasks[currentSubTask+1:]...)
	storage.Projects[currentProject].Tasks[currentTask].SubTasks = subtasks
	if err := saveData(dataFile); err != nil {
		return err
	}

	if len(storage.Projects[currentProject].Tasks[currentTask].SubTasks) > 0 {
		currentSubTask = 0
	} else {
		currentSubTask = -1
	}
	_ = view.SetCursor(0, 1)
	_ = view.SetOrigin(0, 0)
	if err := updateTaskDetailsView(gui, view); err != nil {
		return err
	}
	tv, _ := gui.View("Tasks")
	return updateTasksView(gui, tv)
}

// rearrangeTasks sort the tasks in storage to put the tasks that are done
// after the tasks that are not.
func rearrangeTasks() {
	tasks := make([]Task, 0, len(storage.Projects[currentProject].Tasks))

	for _, task := range storage.Projects[currentProject].Tasks {
		if !task.Done {
			tasks = append(tasks, task)
		}
	}

	for _, task := range storage.Projects[currentProject].Tasks {
		if task.Done {
			tasks = append(tasks, task)
		}
	}

	storage.Projects[currentProject].Tasks = tasks
}

// rearrangeSubTasks sort the sub-tasks in storage to put the sub-tasks that are
// done after the sub-tasks that are not.
func rearrangeSubTasks() {
	subtasks := make([]SubTask, 0, len(storage.Projects[currentProject].Tasks[currentTask].SubTasks))

	for _, subtask := range storage.Projects[currentProject].Tasks[currentTask].SubTasks {
		if !subtask.Done {
			subtasks = append(subtasks, subtask)
		}
	}

	for _, subtask := range storage.Projects[currentProject].Tasks[currentTask].SubTasks {
		if subtask.Done {
			subtasks = append(subtasks, subtask)
		}
	}

	storage.Projects[currentProject].Tasks[currentTask].SubTasks = subtasks
}

func moveCurrentProjectUp(gui *gocui.Gui, view *gocui.View) error {
	if currentProject == 0 {
		// The current project is already at the beginning of the list
		return nil
	}

	indexWhereToInsert := currentProject - 1
	moveProject(currentProject, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentProject--

	x, y := view.Cursor()
	_ = view.SetCursor(x, y-1)

	return updateProjectsView(gui, view)
}

func moveCurrentProjectDown(gui *gocui.Gui, view *gocui.View) error {
	if currentProject+1 >= len(storage.Projects) {
		// The current project is already at the end of the list
		return nil
	}

	indexWhereToInsert := currentProject + 1
	moveProject(currentProject, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentProject++

	x, y := view.Cursor()
	if err := view.SetCursor(x, y+1); err != nil {
		return err
	}

	return updateProjectsView(gui, view)
}

func moveProject(from, to int) {
	currentProjects := storage.Projects
	project := currentProjects[from]

	currentProjects = append(currentProjects[:from], currentProjects[from+1:]...)

	newProjects := make([]Project, to+1)

	copy(newProjects, currentProjects[:to])

	newProjects[to] = project

	newProjects = append(newProjects, currentProjects[to:]...)

	storage.Projects = newProjects
}

func moveCurrentTaskUp(gui *gocui.Gui, view *gocui.View) error {
	if currentTask == 0 {
		// The current task is already at the beginning of the list
		return nil
	}

	indexWhereToInsert := currentTask - 1
	moveTask(currentTask, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentTask--

	x, y := view.Cursor()
	_ = view.SetCursor(x, y-1)

	return updateTasksView(gui, view)
}

func moveCurrentTaskDown(gui *gocui.Gui, view *gocui.View) error {
	if currentTask+1 >= len(storage.Projects[currentProject].Tasks) {
		// The current task is already at the end of the list
		return nil
	}

	indexWhereToInsert := currentTask + 1
	moveTask(currentTask, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentTask++

	x, y := view.Cursor()
	if err := view.SetCursor(x, y+1); err != nil {
		return err
	}

	return updateTasksView(gui, view)
}

func moveTask(from, to int) {
	currentTasks := storage.Projects[currentProject].Tasks
	task := currentTasks[from]

	currentTasks = append(currentTasks[:from], currentTasks[from+1:]...)

	newTasks := make([]Task, to+1)

	copy(newTasks, currentTasks[:to])

	newTasks[to] = task

	newTasks = append(newTasks, currentTasks[to:]...)

	storage.Projects[currentProject].Tasks = newTasks
}

func moveCurrentSubTaskUp(gui *gocui.Gui, view *gocui.View) error {
	if currentSubTask == 0 {
		// The current sub-task is already at the beginning of the list
		return nil
	}

	indexWhereToInsert := currentSubTask - 1
	moveSubTask(currentSubTask, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentSubTask--

	x, y := view.Cursor()
	_ = view.SetCursor(x, y-1)

	return updateTaskDetailsView(gui, view)
}

func moveCurrentSubTaskDown(gui *gocui.Gui, view *gocui.View) error {
	if currentSubTask+1 >= len(storage.Projects[currentProject].Tasks[currentTask].SubTasks) {
		// The current sub-task is already at the end of the list
		return nil
	}

	indexWhereToInsert := currentSubTask + 1
	moveSubTask(currentSubTask, indexWhereToInsert)
	if err := saveData(dataFile); err != nil {
		return err
	}

	currentSubTask++

	x, y := view.Cursor()
	if err := view.SetCursor(x, y+1); err != nil {
		return err
	}

	return updateTaskDetailsView(gui, view)
}

func moveSubTask(from, to int) {
	currentSubTasks := storage.Projects[currentProject].Tasks[currentTask].SubTasks
	subtask := currentSubTasks[from]

	currentSubTasks = append(currentSubTasks[:from], currentSubTasks[from+1:]...)

	newSubTasks := make([]SubTask, to+1)

	copy(newSubTasks, currentSubTasks[:to])

	newSubTasks[to] = subtask

	newSubTasks = append(newSubTasks, currentSubTasks[to:]...)

	storage.Projects[currentProject].Tasks[currentTask].SubTasks = newSubTasks
}
