package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Storage struct {
	Projects []Project
}

func getDefaultStoragePath() string {
	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		userConfigDir = "."
	}

	return filepath.Join(userConfigDir, filepath.ToSlash("godosomething/godosomething.json"))
}

func loadData(filename string) error {
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		if os.IsNotExist(err) && filename == getDefaultStoragePath() {
			// File path is the default value and does not exist.
			// First we create the directory structure and then the file in itself.
			err = os.MkdirAll(filepath.Dir(filename), 0755)
			if err != nil {
				return errors.New(fmt.Sprintf("Error while trying to create directory structure for '%s' : %v", filename, err))
			}
			// We muste add a default content to the file, otherwise the json.Decoder will error out on EOF
			content := []byte("{\"Projects\":[{\"Title\":\"Project n°1\",\"Tasks\":null}]}\n")
			err := ioutil.WriteFile(filename, content, 0644)
			if err != nil {
				return errors.New(fmt.Sprintf("Error while trying to create file '%s' : %v", filename, err))
			}
			file, err = os.Open(filename)
			if err != nil {
				return errors.New(fmt.Sprintf("Error while trying to open file '%s' : %v", filename, err))
			}
		} else if os.IsNotExist(err) {
			return errors.New(fmt.Sprintf("The file '%s' was not found", filename))
		} else {
			return errors.New(fmt.Sprintf("Error while trying to open file '%s' : %v", filename, err))
		}
	}

	decoder := json.NewDecoder(file)
	return decoder.Decode(&storage)
}

func saveData(filename string) error {
	file, err := os.Create(filename)
	defer file.Close()
	if err != nil {
		return errors.New(fmt.Sprintf("Error while trying to create file '%s' : %v", filename, err))
	}

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "    ")
	return encoder.Encode(storage)
}
